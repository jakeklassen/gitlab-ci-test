'use strict';

const expect = require('expect');

describe('Math', () => {
	it('should support number flooring', () => {
		expect(Math.floor(1.2)).toEqual(1);
	});
});
